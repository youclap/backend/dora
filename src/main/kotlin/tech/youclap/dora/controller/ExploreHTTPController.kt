package tech.youclap.dora.controller

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import tech.youclap.dora.service.ExploreService
import tech.youclap.dora.shared.ErrorCode
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.model.HttpError
import com.github.kittinunf.result.mapError
import org.slf4j.LoggerFactory

class ExploreHTTPController(
    private val exploreService: ExploreService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(ExploreHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {
        route("explore") {
            route("featured") {
                route("profiles") {
                    get {
                        featuredProfiles()
                    }
                }
                route("challenges") {
                    get {
                        featuredChallenges()
                    }
                }
            }
            route("trending") {
                route("challenges") {
                    get {
                        trendingChallenges()
                    }
                }
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.featuredProfiles() {
        val result = exploreService.featuredProfiles()
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.featuredChallenges() {
        val result = exploreService.featuredChallenges()
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.trendingChallenges() {
        val result = exploreService.trendingChallenges()
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private val mapServiceError: (ExploreService.Error) -> HttpError = {
        when (it) {
            is ExploreService.Error.Unknown ->
                HttpError(
                    HttpStatusCode.InternalServerError, ErrorCode.UNKNOWN,
                    "Unknown error ${it.cause?.localizedMessage}"
                )
        }
    }
}
