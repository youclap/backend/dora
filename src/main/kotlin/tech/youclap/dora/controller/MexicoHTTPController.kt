package tech.youclap.dora.controller

import com.github.kittinunf.result.mapError
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.patch
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.LoggerFactory
import tech.youclap.dora.model.MexicoExplore
import tech.youclap.dora.service.MexicoService
import tech.youclap.dora.shared.ErrorCode
import tech.youclap.klap.ktor.controller.Controller
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.extension.toHttpError
import tech.youclap.klap.ktor.model.HttpError

class MexicoHTTPController(
    private val service: MexicoService
) : Controller {

    private companion object {
        private val logger = LoggerFactory.getLogger(MexicoHTTPController::class.java)
    }

    override fun boot(routing: Routing) = routing {

        route("dora/mexico/explore") {
            post {
                createMexicoExplore()
            }
            patch {
                updateMexicoExplore()
            }
            delete {
                deleteMexicoExplore()
            }
        }
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.createMexicoExplore() {

        val mexicoExplore = call.receive<MexicoExplore>()

        logger.info("create new mexico explore: $mexicoExplore")

        val result = service.create(mexicoExplore)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.deleteMexicoExplore() {

        val mexicoExplore = call.receive<MexicoExplore>()

        logger.info("delete mexico explore $mexicoExplore")

        val result = service.delete(mexicoExplore)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    private suspend fun PipelineContext<Unit, ApplicationCall>.updateMexicoExplore() {

        val mexicoExplore = call.receive<MexicoExplore>()

        logger.info("update with new mexico explore $mexicoExplore")

        val result = service.update(mexicoExplore)
            .mapError(mapServiceError)

        call.respondResult(result)
    }

    // TODO improve this
    private val mapServiceError: (Exception) -> HttpError = {
        when (it) {
            is MexicoService.Error.ProfileNotFoundByFirebaseID ->
                HttpError(
                    HttpStatusCode.NotFound,
                    ErrorCode.PROFILE_NOT_FOUND,
                    "Profile with firebaseID ${it.firebaseID} not found"
                )
            is MexicoService.Error.ChallengeNotFoundByFirebaseID ->
                HttpError(
                    HttpStatusCode.NotFound,
                    ErrorCode.CHALLENGE_NOT_FOUND,
                    "Challenge with firebaseID ${it.firebaseID} not found"
                )
            else ->
                it.toHttpError()
        }
    }
}
