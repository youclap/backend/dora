package tech.youclap.dora.model

data class FeaturedProfile(
    val profileID: Long,
    val order: Int
)
