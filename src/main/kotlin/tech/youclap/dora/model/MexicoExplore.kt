package tech.youclap.dora.model

data class MexicoExplore(
    val order: Int,
    val section: MexicoSection,
    val type: MexicoExploreType,
    val value: String
)

enum class MexicoSection {
    FEATURED,
    TRENDING
}

enum class MexicoExploreType {
    USER,
    CHALLENGE
}
