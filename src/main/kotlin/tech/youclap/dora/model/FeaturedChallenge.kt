package tech.youclap.dora.model

data class FeaturedChallenge(
    val challengeID: Long,
    val order: Int
)
