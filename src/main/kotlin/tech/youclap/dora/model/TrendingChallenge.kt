package tech.youclap.dora.model

data class TrendingChallenge(
    val challengeID: Long,
    val order: Int
)
