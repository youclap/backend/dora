package tech.youclap.dora.shared

enum class ErrorCode {
    PROFILE_NOT_FOUND,
    CHALLENGE_NOT_FOUND,
    UNKNOWN
}
