@file:SuppressWarnings("TooGenericExceptionCaught")

package tech.youclap.dora.shared

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.transactions.transaction
import com.github.kittinunf.result.Result

suspend fun <T : Any> dbQuery(block: () -> T): Result<T, Exception> {
    return try {
        withContext(Dispatchers.IO) {
            transaction { block() }
        }.run {
            Result.success(this)
        }
    } catch (e: Exception) {
        // TODO maybe we could map this exceptions org.jetbrains.exposed.exceptions
        Result.error(e)
    }
}
