package tech.youclap.dora

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.github.kittinunf.result.Result
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.jackson.jackson
import tech.youclap.klap.ktor.extension.isDev
import tech.youclap.klap.ktor.extension.respondResult
import tech.youclap.klap.ktor.extension.toHttpError

@Suppress("LongMethod", "ComplexMethod")
fun Application.main() {
    install(ContentNegotiation) {
        jackson {
            registerModule(JodaModule())
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            if (isDev) enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    if (isDev) install(CallLogging)

    install(CORS) {
        header("X-Requested-With")
        anyHost()
        allowNonSimpleContentTypes = true
    }

    // Generic exception handling
    install(StatusPages) {
        exception<Throwable> { exception ->
            log.error("❌ unhandled exception", exception)
            call.respondResult(Result.error(exception.toHttpError()))
        }
    }

    installKoin()

    routes()

    setupDatabase()
}
