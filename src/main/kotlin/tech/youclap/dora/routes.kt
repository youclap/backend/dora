package tech.youclap.dora

import io.ktor.application.Application
import io.ktor.routing.routing
import org.koin.ktor.ext.inject
import tech.youclap.dora.controller.ExploreHTTPController
import tech.youclap.dora.controller.MexicoHTTPController
import tech.youclap.dora.service.ExploreService
import tech.youclap.dora.service.MexicoService
import tech.youclap.klap.ktor.controller.HealthCheckController
import tech.youclap.klap.ktor.extension.route

fun Application.routes() {

    val challengeService: ExploreService by inject()

    val mexicoService: MexicoService by inject()

    routing {

        route(HealthCheckController())

        route(ExploreHTTPController(challengeService))

        route(MexicoHTTPController(mexicoService))
    }
}
