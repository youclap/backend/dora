package tech.youclap.dora.store

import com.github.kittinunf.result.Result
import tech.youclap.dora.model.FeaturedChallenge
import tech.youclap.dora.model.FeaturedProfile
import tech.youclap.dora.model.TrendingChallenge

interface ExploreStore {

    suspend fun featuredProfiles(): Result<List<FeaturedProfile>, Error>

    suspend fun featuredChallenges(): Result<List<FeaturedChallenge>, Error>

    suspend fun trendingChallenges(): Result<List<TrendingChallenge>, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
