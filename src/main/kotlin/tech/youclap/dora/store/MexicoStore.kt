@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dora.store

import com.github.kittinunf.result.Result
import tech.youclap.dora.model.FeaturedChallenge
import tech.youclap.dora.model.FeaturedProfile
import tech.youclap.dora.model.TrendingChallenge

interface MexicoStore {

    suspend fun readProfileIDByFirebaseID(firebaseID: String): Result<Long, Error>
    suspend fun readChallengeIDByFirebaseID(firebaseID: String): Result<Long, Error>

    suspend fun createFeaturedProfile(featuredProfile: FeaturedProfile): Result<Long, Error>
    suspend fun createFeaturedChallenge(featuredChallenge: FeaturedChallenge): Result<Long, Error>
    suspend fun createTrendingChallenge(trendingChallenge: TrendingChallenge): Result<Long, Error>

    suspend fun updateFeaturedProfile(featuredProfile: FeaturedProfile): Result<Long, Error>
    suspend fun updateFeaturedChallenge(featuredChallenge: FeaturedChallenge): Result<Long, Error>
    suspend fun updateTrendingChallenge(trendingChallenge: TrendingChallenge): Result<Long, Error>

    suspend fun deleteFeaturedProfile(profileID: Long): Result<Unit, Error>
    suspend fun deleteFeaturedChallenge(challengeID: Long): Result<Unit, Error>
    suspend fun deleteTrendingChallenge(challengeID: Long): Result<Unit, Error>

    sealed class Error(cause: Throwable? = null) : Exception(cause) {
        data class FirebaseProfileNotFound(val firebaseProfileID: String) : Error()
        data class FirebaseChallengeNotFound(val firebaseChallengeID: String) : Error()
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
