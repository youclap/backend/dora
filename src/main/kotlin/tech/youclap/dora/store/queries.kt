package tech.youclap.dora.store

import org.jetbrains.exposed.sql.Query
import org.jetbrains.exposed.sql.select
import tech.youclap.dora.store.table.BlockedProfileTable
import tech.youclap.dora.store.table.HiddenChallengeTable

// TODO try to improve this with union https://github.com/JetBrains/Exposed/issues/402
fun blockedByAuthenticatedProfiles(profileID: Long): Query {
    return BlockedProfileTable
        .slice(BlockedProfileTable.blockedProfileID)
        .select {
            BlockedProfileTable.profileID eq profileID
        }
}

fun blockedAuthenticatedProfiles(profileID: Long): Query {
    return BlockedProfileTable
        .slice(BlockedProfileTable.profileID)
        .select {
            BlockedProfileTable.blockedProfileID eq profileID
        }
}

fun hiddenChallenges(profileID: Long): Query {
    return HiddenChallengeTable
        .slice(HiddenChallengeTable.challengeID)
        .select {
            HiddenChallengeTable.profileID eq profileID
        }
}
