@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dora.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import org.slf4j.LoggerFactory
import tech.youclap.dora.model.FeaturedChallenge
import tech.youclap.dora.model.FeaturedProfile
import tech.youclap.dora.model.TrendingChallenge
import tech.youclap.dora.shared.dbQuery
import tech.youclap.dora.store.table.ChallengeTable
import tech.youclap.dora.store.table.FeaturedChallengeTable
import tech.youclap.dora.store.table.FeaturedProfileTable
import tech.youclap.dora.store.table.ProfileFirebaseTable
import tech.youclap.dora.store.table.TrendingChallengeTable

class MySQLMexicoStore : MexicoStore {

    private companion object {
        private val logger = LoggerFactory.getLogger(MySQLMexicoStore::class.java)
    }

    override suspend fun createFeaturedProfile(featuredProfile: FeaturedProfile): Result<Long, MexicoStore.Error> {
        logger.info("create featured profile: $featuredProfile on DB")

        return dbQuery {
            FeaturedProfileTable
                .insert {
                    it[profileID] = featuredProfile.profileID
                    it[order] = featuredProfile.order
                }
            featuredProfile.profileID
        }
            .mapError(mapError)
    }

    override suspend fun createFeaturedChallenge(
        featuredChallenge: FeaturedChallenge
    ): Result<Long, MexicoStore.Error> {
        logger.info("create featured challenge: $featuredChallenge on DB")

        return dbQuery {
            FeaturedChallengeTable
                .insert {
                    it[challengeID] = featuredChallenge.challengeID
                    it[order] = featuredChallenge.order
                }
            featuredChallenge.challengeID
        }
            .mapError(mapError)
    }

    override suspend fun createTrendingChallenge(
        trendingChallenge: TrendingChallenge
    ): Result<Long, MexicoStore.Error> {
        logger.info("create trending challenge: $trendingChallenge on DB")

        return dbQuery {
            TrendingChallengeTable
                .insert {
                    it[challengeID] = trendingChallenge.challengeID
                    it[order] = trendingChallenge.order
                }
            trendingChallenge.challengeID
        }
            .mapError(mapError)
    }

    override suspend fun deleteFeaturedProfile(profileID: Long): Result<Unit, MexicoStore.Error> {
        logger.info("prepare to delete featuredProfileID $profileID")

        return dbQuery {
            FeaturedProfileTable.deleteWhere {
                FeaturedProfileTable.profileID eq profileID
            }
            Unit
        }
            .mapError(mapError)
    }

    override suspend fun deleteFeaturedChallenge(challengeID: Long): Result<Unit, MexicoStore.Error> {
        logger.info("prepare to delete featuredChallengeID $challengeID")

        return dbQuery {
            FeaturedChallengeTable.deleteWhere {
                FeaturedChallengeTable.challengeID eq challengeID
            }
            Unit
        }
            .mapError(mapError)
    }

    override suspend fun deleteTrendingChallenge(challengeID: Long): Result<Unit, MexicoStore.Error> {
        logger.info("prepare to delete trendingChallengeID $challengeID")

        return dbQuery {
            TrendingChallengeTable.deleteWhere {
                TrendingChallengeTable.challengeID eq challengeID
            }
            Unit
        }
            .mapError(mapError)
    }

    override suspend fun updateFeaturedProfile(featuredProfile: FeaturedProfile): Result<Long, MexicoStore.Error> {
        logger.info("prepare to update featuredProfile $featuredProfile")

        return dbQuery {
            FeaturedProfileTable
                .update({
                    FeaturedProfileTable.profileID eq featuredProfile.profileID
                }) {
                    it[order] = featuredProfile.order
                }
            featuredProfile.profileID
        }
            .mapError(mapError)
    }

    override suspend fun updateFeaturedChallenge(
        featuredChallenge: FeaturedChallenge
    ): Result<Long, MexicoStore.Error> {
        logger.info("prepare to update featuredChallenge $featuredChallenge")

        return dbQuery {
            FeaturedChallengeTable
                .update({
                    FeaturedChallengeTable.challengeID eq featuredChallenge.challengeID
                }) {
                    it[order] = featuredChallenge.order
                }
            featuredChallenge.challengeID
        }
            .mapError(mapError)
    }

    override suspend fun updateTrendingChallenge(
        trendingChallenge: TrendingChallenge
    ): Result<Long, MexicoStore.Error> {
        logger.info("prepare to update trendingChallenge $trendingChallenge")

        return dbQuery {
            TrendingChallengeTable
                .update({
                    TrendingChallengeTable.challengeID eq trendingChallenge.challengeID
                }) {
                    it[order] = trendingChallenge.order
                }
            trendingChallenge.challengeID
        }
            .mapError(mapError)
    }

    override suspend fun readProfileIDByFirebaseID(firebaseID: String): Result<Long, MexicoStore.Error> {
        logger.info("read profileID for firebaseID: $firebaseID")

        return dbQuery {
            ProfileFirebaseTable
                .slice(ProfileFirebaseTable.profileID)
                .select {
                    ProfileFirebaseTable.firebaseID eq firebaseID
                }
                .firstOrNull()
                ?.let {
                    it[ProfileFirebaseTable.profileID]
                }
                ?: throw MexicoStore.Error.FirebaseProfileNotFound(firebaseID)
        }
            .mapError(mapError)
    }

    override suspend fun readChallengeIDByFirebaseID(firebaseID: String): Result<Long, MexicoStore.Error> {
        logger.info("read challengeID for firebaseID: $firebaseID")

        return dbQuery {
            ChallengeTable
                .select {
                    ChallengeTable.firebaseID eq firebaseID
                }
                .firstOrNull()
                ?.let { it[ChallengeTable.id].value }
                ?: throw MexicoStore.Error.FirebaseChallengeNotFound(firebaseID)
        }
            .mapError(mapError)
    }

    private val mapError: (Exception) -> MexicoStore.Error = {
        when (it) {
            is MexicoStore.Error -> it
            else -> MexicoStore.Error.Unknown(it)
        }
    }
}
