@file:SuppressWarnings("TooManyFunctions")

package tech.youclap.dora.store

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.mapError
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SortOrder.ASC
import org.jetbrains.exposed.sql.selectAll
import tech.youclap.dora.model.FeaturedChallenge
import tech.youclap.dora.model.FeaturedProfile
import tech.youclap.dora.model.TrendingChallenge
import tech.youclap.dora.shared.dbQuery
import tech.youclap.dora.store.table.FeaturedChallengeTable
import tech.youclap.dora.store.table.FeaturedProfileTable
import tech.youclap.dora.store.table.TrendingChallengeTable

class MySQLExploreStore : ExploreStore {

    override suspend fun featuredProfiles(): Result<List<FeaturedProfile>, ExploreStore.Error> {
        return dbQuery {
            selectFeaturedProfiles()
        }
            .mapError { ExploreStore.Error.Unknown(it) }
    }

    override suspend fun featuredChallenges(): Result<List<FeaturedChallenge>, ExploreStore.Error> {
        return dbQuery {
            selectFeaturedChallenges()
        }
            .mapError { ExploreStore.Error.Unknown(it) }
    }

    override suspend fun trendingChallenges(): Result<List<TrendingChallenge>, ExploreStore.Error> {
        return dbQuery {
            selectTrendingChallenges()
        }
            .mapError { ExploreStore.Error.Unknown(it) }
    }

    private fun selectFeaturedProfiles(): List<FeaturedProfile> {
        return FeaturedProfileTable
            .selectAll()
            .orderBy(FeaturedProfileTable.order, ASC)
            .map {
                it.toFeaturedProfile()
            }
    }

    private fun selectFeaturedChallenges(): List<FeaturedChallenge> {
        return FeaturedChallengeTable
            .selectAll()
            .orderBy(FeaturedChallengeTable.order, ASC)
            .map {
                it.toFeaturedChallenge()
            }
    }

    private fun selectTrendingChallenges(): List<TrendingChallenge> {
        return TrendingChallengeTable
            .selectAll()
            .orderBy(TrendingChallengeTable.order, ASC)
            .map {
                it.toTrendingChallenge()
            }
    }

    private fun ResultRow.toFeaturedProfile(): FeaturedProfile {
        return FeaturedProfile(
            profileID = this[FeaturedProfileTable.profileID],
            order = this[FeaturedProfileTable.order]
        )
    }

    private fun ResultRow.toFeaturedChallenge(): FeaturedChallenge {
        return FeaturedChallenge(
            challengeID = this[FeaturedChallengeTable.challengeID],
            order = this[FeaturedChallengeTable.order]
        )
    }

    private fun ResultRow.toTrendingChallenge(): TrendingChallenge {
        return TrendingChallenge(
            challengeID = this[TrendingChallengeTable.challengeID],
            order = this[TrendingChallengeTable.order]
        )
    }
}
