package tech.youclap.dora.store.table

import org.jetbrains.exposed.sql.Table

object FeaturedProfileTable : Table("FeaturedProfile") {
    val profileID = long("profileID").references(ProfileTable.id)
    val order = integer("order")
}
