package tech.youclap.dora.store.table

import org.jetbrains.exposed.sql.Table

object TrendingChallengeTable : Table("TrendingChallenge") {
    val challengeID = long("challengeID").references(ChallengeTable.id)
    val order = integer("order")
}
