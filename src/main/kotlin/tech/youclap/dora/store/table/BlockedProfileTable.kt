package tech.youclap.dora.store.table

import org.jetbrains.exposed.sql.Table

object BlockedProfileTable : Table("BlockedProfile") {

    val profileID = long("profileID")
    val userID = long("userID")
    val blockedProfileID = long("blockedProfileID")
    val createdDate = datetime("createdDate")
}
