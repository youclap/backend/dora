package tech.youclap.dora.store.table

import org.jetbrains.exposed.dao.LongIdTable

object ChallengeTable : LongIdTable("Challenge") {

    val profileID = long("profileID")
    val firebaseID = varchar("firebaseID", FIREBASEID_COLUMN_SIZE)
}
