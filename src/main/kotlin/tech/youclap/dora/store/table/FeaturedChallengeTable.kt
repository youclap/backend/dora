package tech.youclap.dora.store.table

import org.jetbrains.exposed.sql.Table

object FeaturedChallengeTable : Table("FeaturedChallenge") {
    val challengeID = long("challengeID").references(ChallengeTable.id)
    val order = integer("order")
}
