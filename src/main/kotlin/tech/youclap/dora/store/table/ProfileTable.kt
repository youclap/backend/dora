package tech.youclap.dora.store.table

import org.jetbrains.exposed.dao.LongIdTable

// To be used by Mexico
object ProfileTable : LongIdTable("Profile")
