package tech.youclap.dora

import io.ktor.application.Application
import io.ktor.application.install
import org.koin.Logger.slf4jLogger
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import tech.youclap.dora.service.ExploreService
import tech.youclap.dora.service.ExploreServiceImpl
import tech.youclap.dora.service.MexicoService
import tech.youclap.dora.service.MexicoServiceImpl
import tech.youclap.dora.store.ExploreStore
import tech.youclap.dora.store.MexicoStore
import tech.youclap.dora.store.MySQLExploreStore
import tech.youclap.dora.store.MySQLMexicoStore

fun Application.installKoin() = install(Koin) {

    slf4jLogger()

    val dependenciesModule = module {

        single<ExploreService> { ExploreServiceImpl(get()) }
        single<ExploreStore> { MySQLExploreStore() }

        single<MexicoService> { MexicoServiceImpl(get()) }
        single<MexicoStore> { MySQLMexicoStore() }
    }

    modules(dependenciesModule)
}
