package tech.youclap.dora.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import com.github.kittinunf.result.mapError
import tech.youclap.dora.store.ExploreStore

class ExploreServiceImpl(
    private val store: ExploreStore
) : ExploreService {

    override suspend fun featuredProfiles(): Result<List<Long>, ExploreService.Error> {
        return store.featuredProfiles()
            .map { profiles ->
                profiles.map { it.profileID }
            }
            .mapError(errorStoreHandler)
    }

    override suspend fun featuredChallenges(): Result<List<Long>, ExploreService.Error> {
        return store.featuredChallenges()
            .map { challenges ->
                challenges.map { it.challengeID }
            }
            .mapError(errorStoreHandler)
    }

    override suspend fun trendingChallenges(): Result<List<Long>, ExploreService.Error> {
        return store.trendingChallenges()
            .map { challenges ->
                challenges.map { it.challengeID }
            }
            .mapError(errorStoreHandler)
    }

    private val errorStoreHandler: (ExploreStore.Error) -> ExploreService.Error = {
        ExploreService.Error.Unknown(it)
    }
}
