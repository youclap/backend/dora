package tech.youclap.dora.service

import com.github.kittinunf.result.Result

interface ExploreService {

    suspend fun featuredProfiles(): Result<List<Long>, Error>

    suspend fun featuredChallenges(): Result<List<Long>, Error>

    suspend fun trendingChallenges(): Result<List<Long>, Error>

    sealed class Error(cause: Throwable?) : Exception(cause) {
        data class Unknown(override val cause: Throwable? = null) : Error(cause)
    }
}
