package tech.youclap.dora.service

import com.github.kittinunf.result.Result
import org.slf4j.LoggerFactory
import tech.youclap.dora.model.FeaturedChallenge
import tech.youclap.dora.model.FeaturedProfile
import tech.youclap.dora.model.MexicoExplore
import tech.youclap.dora.model.MexicoExploreType
import tech.youclap.dora.model.MexicoSection
import tech.youclap.dora.model.TrendingChallenge
import tech.youclap.dora.store.MexicoStore

interface MexicoService {

    suspend fun create(mexicoExplore: MexicoExplore): Result<Long, Exception>
    suspend fun update(mexicoExplore: MexicoExplore): Result<Long, Exception>
    suspend fun delete(mexicoExplore: MexicoExplore): Result<Unit, Exception>

    sealed class Error(cause: Throwable?) : Exception(cause) {
        data class ProfileNotFoundByFirebaseID(val firebaseID: String) : Error(null)
        data class ChallengeNotFoundByFirebaseID(val firebaseID: String) : Error(null)
    }
}

class MexicoServiceImpl(private val store: MexicoStore) : MexicoService {

    private companion object {
        private val logger = LoggerFactory.getLogger(MexicoServiceImpl::class.java)
    }

    override suspend fun create(mexicoExplore: MexicoExplore): Result<Long, Exception> {
        val firebaseID = mexicoExplore.value

        return when (mexicoExplore.type) {
            MexicoExploreType.USER -> {
                val profileID = store.readProfileIDByFirebaseID(firebaseID).get()
                val featuredProfile = mexicoExplore.toFeaturedProfile(profileID)

                logger.info("featured profile: $featuredProfile")
                store.createFeaturedProfile(featuredProfile)
            }

            MexicoExploreType.CHALLENGE -> {
                val challengeID = store.readChallengeIDByFirebaseID(firebaseID).get()
                if (mexicoExplore.section == MexicoSection.FEATURED) {
                    val featuredChallenge = mexicoExplore.toFeaturedChallenge(challengeID)

                    logger.info("featured challenge: $featuredChallenge")
                    store.createFeaturedChallenge(featuredChallenge)
                } else {
                    val trendingChallenge = mexicoExplore.toTrendingChallenge(challengeID)

                    logger.info("trending challenge: $trendingChallenge")
                    store.createTrendingChallenge(trendingChallenge)
                }
            }
        }
    }

    override suspend fun update(mexicoExplore: MexicoExplore): Result<Long, Exception> {
        val firebaseID = mexicoExplore.value

        return when (mexicoExplore.type) {
            MexicoExploreType.USER -> {
                val profileID = store.readProfileIDByFirebaseID(firebaseID).get()
                val newFeaturedProfile = mexicoExplore.toFeaturedProfile(profileID)

                logger.info("update featured profile: $newFeaturedProfile")
                store.updateFeaturedProfile(newFeaturedProfile)
            }

            MexicoExploreType.CHALLENGE -> {
                val challengeID = store.readChallengeIDByFirebaseID(firebaseID).get()
                if (mexicoExplore.section == MexicoSection.FEATURED) {
                    val newFeaturedChallenge = mexicoExplore.toFeaturedChallenge(challengeID)

                    logger.info("update featured challenge: $newFeaturedChallenge")
                    store.updateFeaturedChallenge(newFeaturedChallenge)
                } else {
                    val newTrendingChallenge = mexicoExplore.toTrendingChallenge(challengeID)

                    logger.info("update trending challenge: $newTrendingChallenge")
                    store.updateTrendingChallenge(newTrendingChallenge)
                }
            }
        }
    }

    override suspend fun delete(mexicoExplore: MexicoExplore): Result<Unit, Exception> {
        val firebaseID = mexicoExplore.value

        return when (mexicoExplore.type) {
            MexicoExploreType.USER -> {
                val profileID = store.readProfileIDByFirebaseID(firebaseID).get()

                logger.info("delete featured profile with profileID: $profileID")
                store.deleteFeaturedProfile(profileID)
            }

            MexicoExploreType.CHALLENGE -> {
                val challengeID = store.readChallengeIDByFirebaseID(firebaseID).get()

                if (mexicoExplore.section == MexicoSection.FEATURED) {
                    logger.info("delete featured challenge with challengeID: $challengeID")
                    store.deleteFeaturedChallenge(challengeID)
                } else {
                    logger.info("delete trending challenge with challengeID: $challengeID")
                    store.deleteTrendingChallenge(challengeID)
                }
            }
        }
    }

    private fun MexicoExplore.toFeaturedProfile(profileID: Long): FeaturedProfile {
        return FeaturedProfile(
            profileID = profileID,
            order = this.order
        )
    }

    private fun MexicoExplore.toFeaturedChallenge(challengeID: Long): FeaturedChallenge {
        return FeaturedChallenge(
            challengeID = challengeID,
            order = this.order
        )
    }

    private fun MexicoExplore.toTrendingChallenge(challengeID: Long): TrendingChallenge {
        return TrendingChallenge(
            challengeID = challengeID,
            order = this.order
        )
    }
}
