FROM openjdk:8-jre-alpine

ENV PORT=80

EXPOSE $PORT
VOLUME /tmp

COPY build/libs/dora.jar /app.jar

ENTRYPOINT java -server -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:InitialRAMFraction=2 -XX:MinRAMFraction=2 -XX:MaxRAMFraction=2 -XX:+UseG1GC -XX:MaxGCPauseMillis=100 -XX:+UseStringDeduplication -jar /app.jar
