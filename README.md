# Dora
![Dora the explorer](http://pluspng.com/img-png/png-dora-dora-marquez-dora-the-explorer-png-479.png)

⚠️  Under development... 🚧

Youclap Explore Service

## Service Features

- Featured Profiles
- Featured Challenges
- Trending Challenges
- Calculate trending

##Run with docker image
We are using mysql:5.7 version for this service. You need to create a docker container with mysql:5.7 version.

run it: `docker run -d -p 3306:3306 --name "your-docker-desired-name" -e MYSQL_ROOT_PASSWORD=cenas mysql:5.7`

Execute the container: `docker exec -it "your-docker-desired-name" bash`

Access to mysql console: `mysql -uroot -p`
